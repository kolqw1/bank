package ru.spbstu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.spbstu.dao.AccountDAO;
import ru.spbstu.dao.UserDAO;
import ru.spbstu.entities.AccountEntity;
import ru.spbstu.enums.UserRole;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(value = "/accounts")
public final class AccountController {
    @Autowired
    private AccountDAO accountDAO;
    @Autowired
    private UserDAO userDAO;

    @RequestMapping(value = "/insert", method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority('worker')")
    public final void insert(@RequestBody final AccountEntity accountEntity) {
        accountDAO.insert(accountEntity);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @PreAuthorize("hasAuthority('worker')")
    public final void delete(@RequestParam(value = "id") final Long id) {
        if (accountDAO.selectByID(id).getMoney() <= 0)
            accountDAO.delete(id);
    }

    @RequestMapping(value = "/select", method = RequestMethod.GET)
    public final List<AccountEntity> select(@RequestParam(value = "userID") final Long userID) {
        if (userDAO.getUser().getRole() != UserRole.worker && !Objects.equals(userDAO.getUser().getId(), userID))
            return null;
        return accountDAO.select(userID);
    }
}

