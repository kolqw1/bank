package ru.spbstu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.spbstu.dao.AccountDAO;
import ru.spbstu.dao.OperationDAO;
import ru.spbstu.dao.UserDAO;
import ru.spbstu.entities.AccountEntity;
import ru.spbstu.entities.OperationEntity;
import ru.spbstu.enums.UserRole;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(value = "/operations")
public final class OperationController {
    @Autowired
    private OperationDAO operationDAO;
    @Autowired
    private AccountDAO accountDAO;
    @Autowired
    private UserDAO userDAO;

    @RequestMapping(value = "/insert", method = RequestMethod.PUT)
    @PreAuthorize("isAuthenticated()")
    public final void insert(@RequestBody final OperationEntity operationEntity) {
        if (userDAO.getUser().getRole() != UserRole.worker && !accountDAO.select(userDAO.getUser().getId()).stream().anyMatch(accountEntity -> Objects.equals(accountEntity.getId(), operationEntity.getAccountID())))
            return;
        operationDAO.insert(operationEntity);
        AccountEntity accountEntity = accountDAO.selectByID(operationEntity.getAccountID());
        accountEntity.setMoney(accountEntity.getMoney() + operationEntity.getMoney());
        accountDAO.update(accountEntity);
    }

    @RequestMapping(value = "/select", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated()")
    public final List<OperationEntity> selectAll(@RequestParam(value = "accountID") final Long accountID) {
        if (userDAO.getUser().getRole() != UserRole.worker && !accountDAO.select(userDAO.getUser().getId()).stream().anyMatch(accountEntity -> Objects.equals(accountEntity.getId(), accountID)))
            return null;
        return operationDAO.selectAll(accountID);
    }
}

