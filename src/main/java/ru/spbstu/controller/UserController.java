package ru.spbstu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.spbstu.dao.UserDAO;
import ru.spbstu.entities.UserEntity;

import java.util.List;

@RestController
@RequestMapping(value = "/users")
public final class UserController {
    @Autowired
    private UserDAO userDAO;

    @RequestMapping(value = "/insert", method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority('worker')")
    public final void insert(@RequestBody final UserEntity userEntity) {
        userDAO.insert(userEntity);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('worker')")
    public final void update(@RequestBody final UserEntity userEntity) {
        userDAO.update(userEntity);
    }

    @RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('worker')")
    public final void updatePassword(@RequestBody final UserEntity userEntity) {
        userDAO.updatePassword(userEntity);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    @PreAuthorize("hasAuthority('worker')")
    public final void delete(@RequestParam(value = "id") final Long id) {
        userDAO.delete(id);
    }

    @RequestMapping(value = "/select", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated()")
    public final UserEntity select() {
        return userDAO.getUser();
    }

    @RequestMapping(value = "/selectAll", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('worker')")
    public final List<UserEntity> selectAll() {
        return userDAO.selectAll();
    }
}

