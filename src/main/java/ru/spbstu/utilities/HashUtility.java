package ru.spbstu.utilities;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class HashUtility {

    private HashUtility() { }

    private static String convertToHex(final byte[] data) {
        final StringBuilder sb = new StringBuilder();

        for (final byte aData : data) {
            int halfByte = (aData >>> 4) & 0x0F;
            int twoParts = 0;
            do {
                if ((0 <= halfByte) && (halfByte <= 9))
                    sb.append((char) ('0' + halfByte));
                else
                    sb.append((char) ('a' + (halfByte - 10)));
                halfByte = aData & 0x0F;
            } while (twoParts++ < 1);
        }

        return sb.toString();
    }

    private static String getHash(final String text, final String algorithm) {
        final MessageDigest md;
        try {
            md = MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            final String msg = "NoSuchAlgorithmException thrown in HashUtility";
            throw new RuntimeException(msg);
        }
        md.update(text.getBytes(StandardCharsets.UTF_8), 0, text.length());
        return convertToHex(md.digest());
    }

    public static String sha1(final String string) {
        return getHash(string, "SHA-1");
    }
}