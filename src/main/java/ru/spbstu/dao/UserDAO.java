package ru.spbstu.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.spbstu.entities.UserEntity;
import ru.spbstu.enums.UserRole;
import ru.spbstu.utilities.HashUtility;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public final class UserDAO extends JdbcDaoSupport{
    private UserMapper userMapper = new UserMapper();

    public void insert(final UserEntity user) {
        user.setPassword(HashUtility.sha1(user.getPassword()));
        getJdbcTemplate().update(
                "insert into users (login, password, name, role) values (?, ?, ?, ?)",
                user.getLogin(), user.getPassword(), user.getName(), user.getRole().toString());
    }

    public void update(final UserEntity user) {
        getJdbcTemplate().update(
                "update users SET login=?, name=?, role=? where id=?",
                user.getLogin(), user.getName(), user.getRole().toString(), user.getId());
    }

    public void updatePassword(UserEntity user) {
        user.setPassword(HashUtility.sha1(user.getPassword()));
        getJdbcTemplate().update(
                "update users SET password=? where id=?",
                user.getPassword(), user.getId());
    }

    public void delete(final Long id) {
        getJdbcTemplate().update(
                "delete from users where id=?",
                id);
    }

    public UserEntity getUser() {
        return getJdbcTemplate().queryForObject(
                "select id, login, name, role from users where login=?",
                userMapper, SecurityContextHolder.getContext().getAuthentication().getName());
    }

    public final List<UserEntity> selectAll() {
        return getJdbcTemplate().query(
                "select id, login, name, role from users",
                userMapper);
    }

    private static final class UserMapper implements RowMapper<UserEntity> {
        @Override
        public UserEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
            UserEntity user = new UserEntity();
            user.setId(rs.getLong("id"));
            user.setLogin(rs.getString("login"));
            user.setName(rs.getString("name"));
            user.setRole(Enum.valueOf(UserRole.class, rs.getString("role")));
            return user;
        }
    }
}
