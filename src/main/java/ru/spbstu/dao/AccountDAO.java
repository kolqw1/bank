package ru.spbstu.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import ru.spbstu.entities.AccountEntity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public final class AccountDAO extends JdbcDaoSupport{
    private AccountMapper accountMapper = new AccountMapper();

    public void insert(final AccountEntity account) {
        getJdbcTemplate().update(
                "insert into accounts (userID, money) values (?, ?)",
                account.getUserID(), account.getMoney());
    }

    public void update(final AccountEntity account) {
        getJdbcTemplate().update(
                "update accounts SET money=? where id=?",
                account.getMoney(), account.getId());
    }

    public void delete(final Long id) {
        getJdbcTemplate().update(
                "delete from accounts where id=?",
                id);
    }

    public final List<AccountEntity> select(final Long userID) {
        return 	getJdbcTemplate().query(
                "select * from accounts where userID = ?",
                accountMapper, userID);
    }

    public final AccountEntity selectByID(final Long id) {
        return 	getJdbcTemplate().queryForObject(
                "select * from accounts where id = ?",
                accountMapper, id);
    }

    private static final class AccountMapper implements RowMapper<AccountEntity> {
        @Override
        public AccountEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
            AccountEntity account = new AccountEntity();
            account.setId(rs.getLong("id"));
            account.setUserID(rs.getLong("userID"));
            account.setMoney(rs.getLong("money"));
            return account;
        }
    }
}
