package ru.spbstu.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import ru.spbstu.entities.OperationEntity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public final class OperationDAO extends JdbcDaoSupport{
    private OperationMapper operationMapper = new OperationMapper();

    public void insert(final OperationEntity operation) {
        getJdbcTemplate().update(
                "insert into operations (accountID, money, description) values (?, ?, ?)",
                operation.getAccountID(), operation.getMoney(), operation.getDescription());
    }

    public OperationEntity select(final Long id) {
        return getJdbcTemplate().queryForObject(
                "select * from operations where id=?",
                operationMapper, id);
    }

    public final List<OperationEntity> selectAll(final Long accountID) {
        return getJdbcTemplate().query(
                "select * from operations where accountID=?",
                operationMapper, accountID);
    }

    private static final class OperationMapper implements RowMapper<OperationEntity> {
        @Override
        public OperationEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
            OperationEntity operation = new OperationEntity();
            operation.setId(rs.getLong("id"));
            operation.setMoney(rs.getLong("money"));
            operation.setDescription(rs.getString("description"));
            return operation;
        }
    }
}
