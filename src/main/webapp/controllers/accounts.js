var app = angular.module('app', []);

app.controller('accounts', function ($scope, $http) {
    function getParameterByName(name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $http.get('services/users/select').then(data2 =>  {
        let userID;
        $scope.isWorker = data2.data.role === 'worker';
        if ($scope.isWorker)
            userID = getParameterByName('id')?getParameterByName('id'):data2.data.id;
        else
            userID = data2.data.id;
        $scope.accountNew = {};
        $scope.accountNew.userID = userID;
        $scope.accountNew.money=0;

        const select = () => $http.get('services/accounts/select?userID='+userID).then(data =>  $scope.accounts = data.data);
        $scope.error = false;
        $scope.insert = () => {
            $http.put('services/accounts/insert', $scope.accountNew).then(
                () => {
                    $scope.error = false;
                    select();
                },
                () => $scope.error = true);
        };
        $scope.delete = (i) => {
            $http.delete('services/accounts/delete?id='+$scope.accounts[i].id).then(
                () => {
                    $scope.error = false;
                    select();
                },
                () => $scope.error = true);
        };
        $scope.logout = () => {
            $http.post('/logout').then(() => window.location.href='/');
        };
        select();
    });
});