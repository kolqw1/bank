var app = angular.module('app', []);

app.controller('operations', function ($scope, $http) {
    function getParameterByName(name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    $http.get('services/users/select').then(data2 =>  {
        function clearNew(){
            $scope.operationNew = {};
            $scope.operationNew.accountID = accountID;
            $scope.to='';
            $scope.from='';
        }
        const accountID = getParameterByName('id');
        $scope.isWorker = (data2.data.role === 'worker');
        clearNew();
        const select = () => $http.get('services/operations/select?accountID='+accountID).then(data =>  $scope.operations = data.data);
        $scope.error = false;
        $scope.insert = () => {
            $http.put('services/operations/insert', $scope.operationNew).then(
                () => {
                    $scope.error = false;
                    clearNew();
                    select();
                },
                () => $scope.error = true);
        };
        $scope.move = () => {
            $scope.operationNew.accountID = $scope.to;
            $http.put('services/operations/insert', $scope.operationNew).then(
                () => {
                    $scope.operationNew.accountID = $scope.from;
                    $scope.operationNew.money = -$scope.operationNew.money;
                    $http.put('services/operations/insert', $scope.operationNew).then(
                        () => {
                            $scope.error = false;
                            clearNew();
                            select();
                        },
                        () => $scope.error = true);
                },
                () => $scope.error = true);
        };
        $scope.logout = () => {
            $http.post('/logout').then(() => window.location.href='/');
        };
        select();
    });
});