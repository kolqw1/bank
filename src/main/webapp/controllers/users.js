var app = angular.module('app', []);

app.controller('users', function ($scope, $http) {
    const selectAll = () => $http.get('services/users/selectAll').then(data =>  $scope.users = data.data);
    $scope.error = false;
    $scope.insert = () => {
        $http.put('services/users/insert', $scope.userNew).then(
            () => {
                $scope.error = false;
                selectAll();
                $scope.userNew = {};
            }, 
            () => $scope.error = true);
    };
    $scope.update = (i) => {
        $http.post('services/users/update', $scope.users[i]).then(
            () => {
                $scope.error = false;
                selectAll();
            },
            () => $scope.error = true);
    };
    $scope.updatePassword = (i) => {
        $http.post('services/users/updatePassword', $scope.users[i]).then(
            () => {
                $scope.error = false;
            },
            () => $scope.error = true);
    };
    $scope.delete = (i) => {
        console.log($scope.users[i].id);
        $http.delete('services/users/delete?id='+$scope.users[i].id).then(
            () => {
                $scope.error = false;
                selectAll();
            },
            () => $scope.error = true);
    };
    $scope.logout = () => {
        $http.post('/logout').then(() => window.location.href='/');
    };
    selectAll();
});